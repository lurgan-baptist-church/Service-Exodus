import configparser
import datetime
import logging
import os
import pathlib
import shutil
import sys

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    stream=sys.stdout,
    level=logging.DEBUG,
)

config = configparser.ConfigParser()
config.read("config.ini")

default_config = config["DEFAULT"]

source_dir = default_config["source"]
destination_dir = default_config["destination"]

# CHECK THAT THE SOURCE AND DESTINATION DIRECTORIES EXIST

source_exists = os.path.exists(source_dir)
destination_exists = os.path.exists(destination_dir)

logging.info("\n\n=== SERVICE EXODUS ===")

if not source_exists:
    logging.error(source_dir + " does not exist.")
    exit

if not destination_exists:
    logging.error(destination_dir + " does not exist.")
    exit

# FIND FILES THAT NEED TO BE ORGANISED

today = datetime.datetime.now()
# Files two weeks ago
time_delta = datetime.timedelta(days=14)

process_files_older_than_date = today - time_delta
CONSIDER_PM_AFTER_HOUR = 15

for file in os.listdir(source_dir).sort(key=os.path.getmtime):
    # Skip files that don't end with .mp4
    if not file.endswith(".mp4"):
        logging.debug("Skipping " + file + ". Doesn't end in .mp4.")
        continue

    file_creation_timestamp = os.path.getmtime(os.path.join(source_dir, file))

    file_creation_datetime = datetime.datetime.fromtimestamp(
        file_creation_timestamp)

    # Skip files created more recently than the time delta
    if file_creation_datetime > process_files_older_than_date:
        logging.info("Skipping " + file + ". Not more than 2 weeks old.")
        continue

    day = file_creation_datetime.day
    month = file_creation_datetime.month
    year = file_creation_datetime.year

    hour = file_creation_datetime.hour

    time_ordinal = "PM" if hour >= CONSIDER_PM_AFTER_HOUR else "AM"

    # All services should follow the convention `YYYY-MM-DD [AM/PM].mp4`
    new_file_name = f'{year}-{str(month).zfill(2)}-{str(day).zfill(2)} {time_ordinal}'

    # Check that the dated destination folder exists and create it if it doesn't
    dated_destination_path = os.path.join(
        destination_dir, str(year), str(month).zfill(2))

    pathlib.Path(dated_destination_path).mkdir(parents=True, exist_ok=True)

    source_path = os.path.join(source_dir, file)
    destination_path = os.path.join(dated_destination_path, new_file_name)

    destination_exists = os.path.exists(destination_path + ".mp4")
    counter = 1

    while destination_exists:
        destination_exists = os.path.exists(
            destination_path + "_" + str(counter) + ".mp4")

        if not destination_exists:
            destination_path = destination_path + "_" + str(counter)

        counter = counter + 1

    # shutil uses a copy-then-delete strategy when moving a file between
    # filesystems, which is what will happen most of the time here.
    shutil.move(source_path, destination_path + ".mp4")

    logging.info("Moved " + source_path + " to " + destination_path)
